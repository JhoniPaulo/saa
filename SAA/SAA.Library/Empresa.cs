﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SAA.Library
{
    public class Empresa
    {
        public int id { get; set; }

        public string nome { get; set; }
        public string email { get; set; }
        public string cnpj { get; set; }
        public string senha { get; set; }
        public DateTime dataCadastro { get; set; }

        public List<Anuncio> anuncios { get; set; }

        public Empresa()
        {
            anuncios = new List<Anuncio>();
        }
    }
}
