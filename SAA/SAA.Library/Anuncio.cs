﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SAA.Library
{
    public class Anuncio
    {
        public int id { get; set; }

        public string titulo { get; set; }
        public DateTime dataCriacao { get; set; }

        public string imagem { get; set; }

        public string conteudo { get; set; }

        public Empresa empresa { get; set; }

        public Interesse interesse { get; set; }

        public Anuncio()
        {
            empresa = new Empresa();
        }
    }
}
