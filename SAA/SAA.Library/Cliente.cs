﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SAA.Library
{
    public class Cliente
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string cpf { get; set; }

        public int idade { get; set; }
        public string email { get; set; }

        public List<Preferencia> preferencias { get; set; }

        public string senha { get; set; }

        public DateTime dataCadastro { get; set; }

        public Cliente()
        {
            preferencias = new List<Preferencia>();
        }
    }
}
