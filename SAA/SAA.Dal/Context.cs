﻿using Microsoft.EntityFrameworkCore;
using SAA.Library;
using System;
using System.Collections.Generic;
using System.Text;

namespace SAA.Dal
{
    class Context: DbContext
    {
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Anuncio> Anuncios { get; set; }
        public DbSet<Empresa> Empresas { get; set; }
        public DbSet<Interesse> Interesses { get; set; }
        public DbSet<Preferencia> Preferencias { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=SaaDB;MultipleActiveResultSets=True;Integrated Security=True");
        }
    }
}
