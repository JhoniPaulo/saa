﻿using SAA.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SAA.Dal
{
    public class EmpresaDAL
    {
        static Context ctx = new Context();
        public static Empresa CadastrarEmpresa(Empresa empresa)
        {
            try
            {
                empresa.dataCadastro = DateTime.Now;
                ctx.Empresas.Add(empresa);
                ctx.SaveChanges();
                return empresa;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static Empresa verificaEmpresa(string email) {            
            try
            {                
                return ctx.Empresas.SingleOrDefault(x => x.email == email);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
