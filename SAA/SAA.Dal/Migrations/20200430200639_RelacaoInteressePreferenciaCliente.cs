﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SAA.Dal.Migrations
{
    public partial class RelacaoInteressePreferenciaCliente : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Preferencias",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    clienteid = table.Column<int>(nullable: true),
                    interesseid = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Preferencias", x => x.id);
                    table.ForeignKey(
                        name: "FK_Preferencias_Clientes_clienteid",
                        column: x => x.clienteid,
                        principalTable: "Clientes",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Preferencias_Interesses_interesseid",
                        column: x => x.interesseid,
                        principalTable: "Interesses",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Preferencias_clienteid",
                table: "Preferencias",
                column: "clienteid");

            migrationBuilder.CreateIndex(
                name: "IX_Preferencias_interesseid",
                table: "Preferencias",
                column: "interesseid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Preferencias");
        }
    }
}
