﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SAA.Dal.Migrations
{
    public partial class RelacaoInteresseAnuncio : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "interesseid",
                table: "Anuncios",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Interesses",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    titulo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Interesses", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Anuncios_interesseid",
                table: "Anuncios",
                column: "interesseid");

            migrationBuilder.AddForeignKey(
                name: "FK_Anuncios_Interesses_interesseid",
                table: "Anuncios",
                column: "interesseid",
                principalTable: "Interesses",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Anuncios_Interesses_interesseid",
                table: "Anuncios");

            migrationBuilder.DropTable(
                name: "Interesses");

            migrationBuilder.DropIndex(
                name: "IX_Anuncios_interesseid",
                table: "Anuncios");

            migrationBuilder.DropColumn(
                name: "interesseid",
                table: "Anuncios");
        }
    }
}
