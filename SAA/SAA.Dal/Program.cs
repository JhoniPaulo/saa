﻿using SAA.Library;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SAA.Dal
{
    class Program
    {
        static void Main(string[] args)
        {

            Context ctx = new Context();

            bool cadastrarCliente()
            {
                try
                {
                    Cliente cliente = new Cliente();
                    Console.WriteLine("Digite as informações solicitadas e presione enter.");
                    Console.WriteLine("");
                    Console.WriteLine("Digite seu Nome:");
                    Console.WriteLine("");
                    cliente.nome = Console.ReadLine();
                    Console.WriteLine("Digite seu Cpf:");
                    Console.WriteLine("");
                    cliente.cpf = Console.ReadLine();
                    Console.WriteLine("Digite a sua Idade:");
                    Console.WriteLine("");
                    cliente.idade = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Digite seu Email:");
                    Console.WriteLine("");
                    cliente.email = Console.ReadLine();
                    Console.WriteLine("Digite sua Senha:");
                    Console.WriteLine("");
                    cliente.senha = Console.ReadLine();

                    ClienteDAL.CadastrarCliente(cliente);
                    Console.WriteLine(cliente.nome + ", seu cadastrado foi realizado com sucesso!");
                    return true;
                } catch (Exception) { throw; }
            }

            bool cadastrarEmpresa()
            {
                try
                {
                    Empresa empresa = new Empresa();
                    Console.WriteLine("Digite as informações solicitadas e presione enter.");
                    Console.WriteLine("");
                    Console.WriteLine("Digite o Nome da Empresa:");
                    Console.WriteLine("");
                    empresa.nome = Console.ReadLine();
                    Console.WriteLine("Digite o Cnpj:");
                    Console.WriteLine("");
                    empresa.cnpj = Console.ReadLine();
                    Console.WriteLine("Digite um Email Empresarial:");
                    Console.WriteLine("");
                    empresa.email = Console.ReadLine();
                    Console.WriteLine("Digite sua Senha:");
                    Console.WriteLine("");
                    empresa.senha = Console.ReadLine();

                    EmpresaDAL.CadastrarEmpresa(empresa);
                    Console.WriteLine(empresa.nome + ", seu cadastrado foi realizado com sucesso!");
                    return true;
                }
                catch (Exception) { throw; }
            }

            int? cadastrarInteresse()
            {
                Console.WriteLine("Digite o nome da Categoria:");
                Console.WriteLine("");
                string categoria = Console.ReadLine();
                int? interesseId = InteresseDAL.CadastrarInteresse(categoria);
                if (interesseId != null) Console.WriteLine("Categoria cadastrada com sucesso!");
                return interesseId;
            }

            bool cadastrarAnuncio()
            {
                try
                {
                    Anuncio anuncio = new Anuncio();
                    Console.WriteLine("Digite as informações solicitadas e presione enter.");
                    Console.WriteLine("");
                    Console.WriteLine("Digite seu email Empresarial:");
                    Console.WriteLine("");
                    var email = Console.ReadLine();
                    if (String.IsNullOrEmpty(email)) return false;                   

                    Console.WriteLine("Digite o Titulo do Anúncio:");
                    Console.WriteLine("");
                    anuncio.titulo = Console.ReadLine();
                    Console.WriteLine("Digite o link da imagem:");
                    Console.WriteLine("");
                    anuncio.imagem = Console.ReadLine();
                    Console.WriteLine("Digite o Conteúdo do Anúncio:");
                    Console.WriteLine("");
                    anuncio.conteudo = Console.ReadLine();
                    Console.WriteLine("Deseja escolher uma categoria já existente para o anúncio ou criar uma nova? Sugerimos que confira as existentes antes ...");
                    Console.WriteLine("Digite 1 para conferir e 2 para criar uma nova:");
                    Console.WriteLine("");
                    var opcoes = Convert.ToInt32(Console.ReadLine());

                    if (opcoes == 1)
                    {
                        Console.WriteLine("Digite o numero referente a categoria do anúncio ou S para sair e cadastrar uma nova categoria:");
                        Console.WriteLine("");
                        var interesses = InteresseDAL.ListarInteresses();
                        foreach (var x in interesses)
                        {
                            Console.WriteLine(x.id + " - " + x.titulo);
                            Console.WriteLine("");
                        }

                        var cat = Console.ReadLine();

                        if(cat == "s" || cat == "S")
                        {
                            int? interesseId = cadastrarInteresse();
                            if (interesseId == null) { Console.WriteLine("Algo deu Errado, Interesse não cadastrado!"); }
                            AnuncioDAL.CadastrarAnuncio(anuncio, email, interesseId);
                            Console.WriteLine("Anúncio cadastrado com sucesso!");
                        }
                        else
                        {
                            AnuncioDAL.CadastrarAnuncio(anuncio, email, Convert.ToInt32(cat));
                            Console.WriteLine("Anúncio cadastrado com sucesso!");
                        }
                    }
                    else if (opcoes == 2)
                    {
                        int? interesseId = cadastrarInteresse();
                        if (interesseId == null) { Console.WriteLine("Algo deu Errado, Interesse não cadastrado!"); }
                        AnuncioDAL.CadastrarAnuncio(anuncio, email, interesseId);
                        Console.WriteLine("Anúncio cadastrado com sucesso!");
                    }   
                    return true;
                }
                catch (Exception) { throw; }
            }

            void ImprimirAnuncios(IQueryable<Anuncio> anuncios){
                foreach (var anuncio in anuncios)
                {
                    Console.WriteLine("Titulo:" + anuncio.titulo);
                    Console.WriteLine("");
                    Console.WriteLine("Imagem:" + anuncio.imagem);
                    Console.WriteLine("");
                    Console.WriteLine("Conteúdo: " + anuncio.conteudo);
                    Console.WriteLine("");
                    Console.WriteLine("Empresa:" + anuncio.empresa.nome);
                    Console.WriteLine("");
                    if(!String.IsNullOrEmpty(anuncio.interesse.titulo)) Console.WriteLine("Categoria:" + anuncio.interesse.titulo);
                    Console.WriteLine("");
                    Console.WriteLine("");
                    Console.WriteLine("");
                }
            }

            void listarTodosAnuncios()
            {
                try
                {
                    ImprimirAnuncios(AnuncioDAL.ListarAnuncios());
                }
                catch (Exception)
                {

                    throw;
                }
            }

            bool listarAnunciosPorEmpresa()
            {
                try
                {
                    Console.WriteLine("Digite as informações solicitadas e presione enter.");
                    Console.WriteLine("");
                    Console.WriteLine("Digite seu email Empresarial:");
                    Console.WriteLine("");
                    var email = Console.ReadLine();
                    if (String.IsNullOrEmpty(email)) return false;

                    ImprimirAnuncios(AnuncioDAL.ListarAnunciosPorEmpresa(email));
                    return true;
                }
                catch (Exception)
                {

                    throw;
                }
            }

            bool cadastrarInteresses()
            {
                try
                {
                    InteresseDAL.CadastrarInteresses();
                    return true;
                }
                catch (Exception)
                {

                    throw;
                }
            }

            bool cadastrarPreferencias()
            {
                Console.WriteLine("Digite as informações solicitadas e presione enter.");
                Console.WriteLine("");
                Console.WriteLine("Digite seu email:");
                Console.WriteLine("");
                var email = Console.ReadLine();
                if (String.IsNullOrEmpty(email)) return false;

                Console.WriteLine("Digite os numeros referentes as suas preferencias separados por vírgula e sem espacos. Ex: 1,2,3");
                Console.WriteLine("");

                List<Interesse> interesses = InteresseDAL.ListarInteresses();

                foreach (var interesse in interesses)
                {
                    Console.WriteLine(interesse.id + " - " + interesse.titulo);
                    Console.WriteLine("");
                }

                var preferenciasConsole = Console.ReadLine();
                var preferencias = preferenciasConsole.Split(",");

                List<int> interessesId = new List<int>();

                foreach (var item in preferencias)
                {
                    if(!String.IsNullOrEmpty(item))
                        interessesId.Add(Convert.ToInt32(item));
                }

                PreferenciaDAL.CadastrarPreferencias(interessesId, email);

                Console.WriteLine("Preferências cadastradas com sucesso!");

                return true;
            }

            int encerra = 0;
            do
            {
                Console.WriteLine("Escolha uma opção, digite seu número e pressione enter ou somente pressione enter para sair:");
                Console.WriteLine("");
                Console.WriteLine("1 - Cadastre-se");
                Console.WriteLine("");
                Console.WriteLine("2 - Cadastrar empresa");                
                Console.WriteLine("");
                Console.WriteLine("3 - Criar Anúncios");                
                Console.WriteLine("");
                Console.WriteLine("4 - Visualizar Anúncios");
                Console.WriteLine("");
                Console.WriteLine("5 - Listar meus Anúncios");
                Console.WriteLine("");
                Console.WriteLine("6 - Cadastrar Preferencias");


                try { 
                    var opcoes = Convert.ToInt32(Console.ReadLine());

                    switch (opcoes)
                    {
                        case (0): cadastrarInteresses(); break;

                        case (1): cadastrarCliente(); break;

                        case (2): cadastrarEmpresa(); break;

                        case (3): cadastrarAnuncio(); break;

                        case (4):
                            Console.WriteLine("Digiteu seu Email:");
                            Console.WriteLine("");
                            
                            var email = Console.ReadLine();

                            if (PreferenciaDAL.TemPreferenciasCliente(email))
                            {
                                List<int> interessesId = PreferenciaDAL.ListarInteressesPorCliente(email);
                                ImprimirAnuncios(AnuncioDAL.ListarAnunciosPorUsuario(interessesId));
                            }
                            else
                            {
                                listarTodosAnuncios();
                            }
                        break;
                        case (5): listarAnunciosPorEmpresa(); break;

                        case (6): cadastrarPreferencias(); break;
                    }

                } catch (Exception e) { Console.WriteLine(e);  encerra = 1; }

            } while (encerra == 0);

        }
    }
}
